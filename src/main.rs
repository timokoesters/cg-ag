mod color;
mod error;
mod ray;
mod vec3;

pub use color::Color;
pub use error::{Error, Result};
pub use ray::Ray;
use std::fs::File;
use std::io::{BufWriter, Write};
pub use vec3::{Point3, Vec3};

fn main() {
    // Image

    let image_width = 400_u32;
    let aspect_ratio = 16.0 / 9.0;
    let image_height = (image_width as f64 / aspect_ratio) as u32;

    let viewport_height = 2.0;
    let viewport_width = aspect_ratio * viewport_height;
    let focal_length = 1.0;

    let origin = Point3::new(0.0, 0.0, 0.0);
    let horizontal = Vec3::new(viewport_width, 0.0, 0.0);
    let vertical = Vec3::new(0.0, viewport_height, 0.0);
    let lower_left_corner =
        origin - 1.0 / 2.0 * horizontal - 1.0 / 2.0 * vertical - Vec3::new(0.0, 0.0, focal_length);

    // Render
    let mut file = BufWriter::new(File::create("output.ppm").unwrap());
    writeln!(&mut file, "P3\n{image_width} {image_height}\n255").unwrap();

    for j in (0..image_height).rev() {
        eprint!("\rScanlines remaining: {j:3}");
        for i in 0..image_width {
            let u = i as f64 / (image_width - 1) as f64;
            let v = j as f64 / (image_height - 1) as f64;

            let ray = Ray {
                origin,
                dir: lower_left_corner + u * horizontal + v * vertical - origin,
            };
            let pixel_color = ray_color(&ray);
            pixel_color.write(&mut file).unwrap();
        }
    }
    eprintln!("\nDone!");
}

fn ray_color(r: &Ray) -> Color {
    if hit_sphere(r, Point3::new(0.0, 0.0, -1.0), 0.5) {
        return Color::new(1.0, 0.0, 0.0).unwrap();
    }

    let dir_n = r.dir.normalized();
    let t = 0.5 * (dir_n.y() + 1.0);

    let a = Color::new(1.0, 1.0, 1.0).unwrap();
    let b = Color::new(0.5, 0.7, 1.0).unwrap();
    a.lerp(&b, t).unwrap()
}

fn hit_sphere(r: &Ray, center: Point3, radius: f64) -> bool {
    let oc = r.origin - center;
    let a = r.dir.dot(&r.dir);
    let b = 2.0 * oc.dot(&r.dir);
    let c = oc.dot(&oc) - radius * radius;
    let discriminant = b * b - 4.0 * a * c;
    discriminant > 0.0
}
