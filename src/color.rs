use crate::{Error, Result, Vec3};
use std::io::Write;

pub struct Color {
    /// All entries are between 0 and 1.
    vec: Vec3,
}

impl Color {
    pub fn new(r: f64, g: f64, b: f64) -> Result<Self> {
        if r < 0.0 || r > 1.0 || g < 0.0 || g > 1.0 || b < 0.0 || b > 1.0 {
            return Err(Error::ColorError);
        }

        Ok(Self {
            vec: Vec3::new(r, g, b),
        })
    }

    pub fn write(&self, out: &mut impl Write) -> std::io::Result<()> {
        let ir = (255.0 * self.vec[0]).round() as i32;
        let ig = (255.0 * self.vec[1]).round() as i32;
        let ib = (255.0 * self.vec[2]).round() as i32;

        writeln!(out, "{ir} {ig} {ib}")
    }

    pub fn lerp(&self, other: &Color, t: f64) -> Result<Color> {
        if t < 0.0 || t > 1.0 {
            return Err(Error::ColorError);
        }
        // Valid because all values are between 0 and 1
        Ok(Color {
            vec: ((1.0 - t) * self.vec + t * other.vec),
        })
    }
}
